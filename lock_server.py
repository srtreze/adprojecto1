#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Aplicações distribuídas - Projeto 1 - lock_server.py
Grupo:
Números de aluno:
"""

# Zona para fazer importação

import time as t,pickle,sock_utils,sys,copy,thread

###############################################################################

class resource_lock:
    def __init__(self,K):
        """
        Define e inicializa as características de um LOCK num recurso.
        """
        self.lock_state = False
        self.lock_counter = 0
        self.resource_owner = 0
        self.locktime_by_owner = []
        self.time_expire = 0
        self.max_clients=K

    def lock(self, client_id, time_limit):
        """
        Bloqueia o recurso se este não estiver bloqueado ou mantém o bloqueio
        se o recurso estiver bloqueado pelo cliente client_id. Neste caso renova
        o bloqueio do recurso até time_limit.
        Retorna True se bloqueou o recurso ou False caso contrário.
        """

        if (not(client_id in self.locktime_by_owner)) and self.max_clients>len(self.locktime_by_owner):
            if len(self.locktime_by_owner)==0 and self.resource_owner==0:
                self.lock_counter += 1
                self.lock_state = True
                self.time_expire=time_limit
            else:
                self.lock_counter += 1
                self.locktime_by_owner.append(client_id)
            return True
        elif client_id in self.locktime_by_owner:
            self.lock_counter += 1

            return True

        return False


    def urelease(self):
        """
        Liberta o recurso incondicionalmente, alterando os valores associados
        ao bloqueio.
        """
        self.lock_state = False
        self.time_expire = 0

    def release(self, client_id):
        """
        Liberta o recurso se este foi bloqueado pelo cliente client_id,
        retornando True nesse caso. Caso contrário retorna False.
        """
        if client_id in self.locktime_by_owner:
            self.locktime_by_owner.pop(client_id, None)
            if self.locktime_by_owner == {}:
                self.lock_state = False
            return True
        return False

    def test(self):
        """
        Retorna o estado de bloqueio do recurso.
        """
        return self.lock_state
    
    def stat(self):
        """
        Retorna o número de vezes que este recurso já foi bloqueado.
        """
        return self.lock_counter

    def stat_k(self):
        """
        Retorna o número de bloqueios em simultâneo em K.
        """
        return len(self.locktime_by_owner)

    def time(self):
        return self.time_expire

        
###############################################################################

class lock_pool:
    def __init__(self, N,K,time):
        """
        Define um array com um conjunto de locks para N recursos. Os locks podem
        ser manipulados pelos métodos desta classe.
        Define também K, o valor máximo do número de bloqueios simultâneos a um recurso qualquer.
        """
        self.time=0
        self.lock_pool_array = []
        for i in range(N):
            self.lock_pool_array.append(resource_lock(K))

    def clear_expired_locks(self):
        """
        Verifica se os recursos que estão bloqueados ainda estão dentro do tempo
        de concessão do bloqueio. Liberta os recursos caso o seu tempo de
        concessão tenha expirado.
        """
        for resource_lock in self.lock_pool_array:
            if resource_lock.time_expire < t.time() and resource_lock.time_expire!=0:
                resource_lock.urelease()
                print "Expirou o actual"
            if len(resource_lock.locktime_by_owner)>0:
                print "mudei para o cliente: " + resource_lock.locktime_by_owner[0]
                resource_lock.lock(resource_lock.locktime_by_owner[0],t.time(),self.time)
                resource_lock.locktime_by_owner.pop(0)



    def lock(self, resource_id, client_id, time_limit):
        """
        Tenta bloquear o recurso resource_id pelo cliente client_id, até ao
        instante time_limit.
        O bloqueio do recurso só é possível se K ainda não foi excedido para este
        recurso. É aconselhável implementar um método _try_lock para vericar estas condições. 
        Retorna True em caso de sucesso e False caso contrário.
        """
        return self.lock_pool_array[resource_id].lock(client_id, time_limit)

    def release(self, resource_id, client_id):
        """
        Liberta o bloqueio sobre o recurso resource_id pelo cliente client_id.
        True em caso de sucesso e False caso contrário.
        """
        return self.lock_pool_array[resource_id].release(client_id)

    def test(self,resource_id):
        """
        Retorna True se o recurso resource_id estiver bloqueado e False caso
        contrário.
        """
        return self.lock_pool_array[resource_id].test()

    def stat(self,resource_id):
        """
        Retorna o número de vezes que o recurso resource_id já foi bloqueado.
        """
        return self.lock_pool_array[resource_id].stat()

    def stat_k(self,resource_id):
        """
        Retorna o número de bloqueios simultâneos no recurso resource_id.
        """
        return self.lock_pool_array[resource_id].stat_k()

    def __repr__(self):
        """
        Representação da classe para a saída standard. A string devolvida por
        esta função é usada, por exemplo, se uma instância da classe for
        passada à função print.
        """
        output = ""
        counter = 0
        for lock in self.lock_pool_array:
            output += "recurso " + str(counter) + " bloqueado pelo cliente " + str(lock.resource_owner) + "\n"
            counter += 1
        return output

###############################################################################

# código do programa principal





if(len(sys.argv)>4):
    HOST = ''
    PORT = int(sys.argv[1])
    resource_number=int(sys.argv[2])
    max_clients=int(sys.argv[3])
    resource_time=int(sys.argv[4])
else:
    HOST = ''
    PORT = 9999
    resource_number=10
    resource_time=10
    max_clients=5
    print "A utiizar os parametros padrão"
print "Porta: " + str(PORT)
print "Recursos: " + str(resource_number) + " Tempo: " + str(resource_time)
print "Bloqueios em simultâneo: " + str(max_clients)

lp = lock_pool(resource_number,max_clients,resource_time)

def expireChecker():
    while(True):
        print "Checking locks"
        lp.clear_expired_locks()
        t.sleep(1)

thread.start_new_thread(expireChecker, ())
msgcliente = []
ret = []
sock=sock_utils.create_tcp_server_socket(HOST,PORT,1)
while True:
    (conn_sock, addr) = sock.accept()
    print 'ligado a %s', addr
#    try:
    msg = sock_utils.receive_all(conn_sock,1024)
    msg_unp = pickle.loads(msg)
    print 'recebi %s' % msg_unp
    msg_unp[1]=int(msg_unp[1])
    if(len(msg_unp)>2):
        msg_unp[2]=int(msg_unp[2])

    if msg_unp[1] > len(lp.lock_pool_array):
            msg_pronta_enviar = 'UNKNOWN RESOURCE'
    else:
        lp.clear_expired_locks()
        if(msg_unp[0] == 'LOCK'):
            if lp.lock(msg_unp[1], msg_unp[2], t.time() + resource_time):
                msg_pronta_enviar = 'OK'
            else:
                msg_pronta_enviar = 'NOK'

        elif(msg_unp[0] == 'RELEASE'):
            if lp.release(msg_unp[1], msg_unp[2]):
                msg_pronta_enviar = 'OK'
            else:
                msg_pronta_enviar = 'NOK'

        elif(msg_unp[0] == 'TEST'):
            if lp.test(msg_unp[1]):
                msg_pronta_enviar = 'LOCKED'
            else:
                msg_pronta_enviar = 'UNLOCKED'

        elif(msg_unp[0] == 'STATS'):
            msg_pronta_enviar = lp.stat(msg_unp[1])

        elif(msg_unp[0] == 'STATS_K'):
            msg_pronta_enviar = lp.stat_k(msg_unp[1])


        else:
            print "ERROR ERROR ERROR ABORT ABORT ABORT :D"
            msg_pronta_enviar = "cant do op"



        msg_pronta_enviar = pickle.dumps(msg_pronta_enviar,-1)
        conn_sock.sendall(msg_pronta_enviar)
        conn_sock.close()
#    except:
        #TODO
        # sem o try except, pode dar "RuntimeError: dictionary changed size during iteration"
        # porque o dicionário de um dos recursos pode mudar enquando se tá a executar lp.clear_expired_locks()
#        msg_pronta_enviar = "There was a problem with request"
#        conn_sock.sendall(pickle.dumps(msg_pronta_enviar,-1))
#        print "Unexpected error:", sys.exc_info()[0]
#        conn_sock.close()
sock.close()
