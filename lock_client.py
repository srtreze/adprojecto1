#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Aplicações distribuídas - Projeto 1 - lock_client.py
Grupo:
Números de aluno:
"""
# Zona para fazer imports

import sys
import net_client as n

# Programa principal

client_commands = ["LOCK","RELEASE","TEST", "STATS", "STATS_K" ,"EXIT","AUTO"]
client_id_commands = ["LOCK","RELEASE"]
if len(sys.argv) > 3:
    HOST = sys.argv[1]
    PORT = int(sys.argv[2])
    ID = int(sys.argv[3])

    #Unit testing
    def startUnitTest(ID):
        f = open("unitTest", "r")
        commandList = []
        for a in f.readlines():
            temp = a.split()
            # if temp[0] in ["LOCK", "RELEASE"]:
            #     temp.append(ID)
            commandList.append(temp)
        print "A começar unit testing:"
        for a in commandList:
            print "Enviado: " + str(a[:3])
            lserver = n.server(HOST, PORT)
            lserver.connect()
            print 'Recebi:' + str(lserver.send_receive(a[:3])) + " -> " + str(a[3])
            lserver.close()

    while True:
        msg = raw_input("Comando: ")
        msg=msg.split(" ")
        if msg[0] == "EXIT":
                sys.exit()
        if msg[0] == "AUTO":
            while(True):
                startUnitTest(ID)
        #Comandos que percisam de client_ID
        if msg[0] in client_id_commands and len(msg)==2:
            msg.append(ID)
        #Comandos que nao percisam de client_ID
        if msg[0] in client_commands and len(msg)>1:
            lserver=n.server(HOST,PORT)
            lserver.connect()
            print 'Recebi ', lserver.send_receive(msg)
            lserver.close()
        else:
            "Strange command try again"

else:
    print "Sem argumentos ou argumentos incompletos"




# Programa principal

