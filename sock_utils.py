# -*- coding: utf-8 -*-

import socket as s

def create_tcp_server_socket(address, port, queue_size):
    """
    Esta função serve para criar uma socket de servidor TCP onde poderão posteriormente ser
aceites ligações de clientes.
    :param address: será o endereço anfitrião à qual a socket ficará vinculada
    :param port: será a porta onde o servidor atenderá novos pedidos de ligação
    :param queue_size:  define o número máximo de pedidos de ligação em espera. (ver função listen dos
objetos da classe socket)
    :return: listener_socket
    """

    listener_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
    listener_socket.setsockopt(s.SOL_SOCKET, s.SO_REUSEADDR, 1)
    listener_socket.bind((address, int(port)))
    listener_socket.listen(int(queue_size))

    return listener_socket

def create_tcp_client_socket():
    """
    Esta função serve para criar uma socket de ligação para o cliente comunicar com um servidor.
    :param address: será o endereço do servidor onde o cliente se ligará
    :param port: será a porta onde o servidor atende pedidos de ligação
    :return: client_socket
    """

    client_socket = s.socket(s.AF_INET, s.SOCK_STREAM)

    return client_socket

def receive_all(socket, length):
    """
    Esta função deverá receber no máximo lenght bytes através da socket. Devem considerar o
caso de a socket remota fechar a ligação antes de enviar o número esperado de bytes.
    :param socket: será a socket de ligação para ler os dados
    :param length: determina o número de bytes que devem ser lidos e devolvidos
    :return: dados recebidos
    """

    socket.settimeout(5)
    try:
        return socket.recv(int(length))
    except socket.timeout as s:
        socket.settimeout(None)
        print "Perda de ligação\n", s